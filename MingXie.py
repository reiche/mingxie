import sys
import datetime

import math
import scipy.special as sp
import numpy as np


class MingXie():
    def __init__(self,):

        # constants
        self.Eph1A=12.39842   # photon energy of 1 Angstrom

        # the fixed parameters
        self.lu=0.015
        self.Kmin=0.8
        self.Kmax=3
        self.Emin=2.0
        self.Emax=7.0
        self.lmod=4
        self.isRound=True
        self.rold=0

        # dynamic variables
        self.planar=True
        self.E=5.8
        self.K=1.2


        
       
    # setting the parameters which aren't changed.    
    def init(self,lu,Emin,Emax,Kmin,Kmax,lmod,isRound):
        self.lu=lu
        self.Emin=Emin
        self.Emax=Emax
        self.Kmin=Kmin
        self.Kmax=Kmax
        self.lmod=lmod
        self.isRound=isRound



    def getResonantEnergy(self):
        gam=self.E*1e3/0.511
        aw2=self.K*self.K
        if self.planar:
            aw2*=0.5
        lam=self.lu*0.5/gam/gam*(1+aw2)
        Eph=self.Eph1A*1e-10/lam
        return Eph

    # if a new wavelength is desired, there are following strategies on how to tune
    # 'K' changes K till limit and then change energy
    # 'E' changes energy till limit then K
    # anything else optimize for maximum K and energy 
    # returns false if no solution has been found

    def findResonance(self,Eph,strategy,it=1):

        
        lam=self.Eph1A/Eph*1e-10
        gam=self.E*1e3/0.511
        awmin=self.Kmin*self.Kmin
        awmax=self.Kmax*self.Kmax
        aw0 = self.K*self.K
        if self.planar:
            awmin*=0.5
            awmax*=0.5
            aw0*=0.5

    

        if 'K' is strategy:
            val=lam*2*gam*gam/self.lu-1   # this should be the desired aw2 value
            if val < awmin:
                if it is 0:
                    return False
                else:
                    self.K=self.Kmin
                    return self.findResonance(Eph,'E',0)
            elif val > awmax:
                if it is 0:
                    return False
                else:
                    self.K=self.Kmax
                    return self.findResonance(Eph,'E',0)
            else:
                if self.planar:
                    val*=2                    
                self.K=math.sqrt(val)
                self.Eph=Eph
                return True
        elif 'E' is strategy:
            val=math.sqrt(self.lu*0.5/lam*(1+aw0))*0.511e-3
            if val < self.Emin:
                if it is 0:
                    return False
                else:
                    self.E=self.Emin
                    return self.findResonance(Eph,'K',0)
            elif val > self.Emax:
                if it is 0:
                    return False
                else:
                    self.E=self.Emax
                    return self.findResonance(Eph,'K',0)
            else:
                self.E=val
                self.Eph=Eph
                return True
        else:
            self.K=self.Kmax
            return self.findResonance(Eph,'E',1)
            

    def getMingXie(self,cur,emit,spread,beta):

        
        gamma=self.E*1e3/0.511
        K=self.K
        if self.planar:
            K=K/math.sqrt(2)

        Eph=self.getResonantEnergy()
        lam=1e-10*self.Eph1A/Eph

        # beam parameters
        cur=cur*1e3
        siggam=spread*0.01*gamma
        emit=emit*1e-9

        return self.MingXie(lam,K,self.lu,beta,gamma,cur,siggam,emit,1,self.planar)


    def getLoss(self,prof,s):

        self.loss=prof*0   # profile is already sampled with 3000 samples
        wake=np.interp(s,self.s,self.wake)   
#        self.loss=wake
#        return
        wake[0]*=0.5
        


        ns=s.size
        ds=s[1]-s[0]
        ne=ds/3e8/1.6e-19


        dds=self.s[1]-self.s[0]
        nns=self.s.size


        for i in range(0,ns):
            self.loss[i]=np.sum(prof[i:ns]*wake[0:ns-i])
        self.loss=self.loss*ne*1e-3  #  convert to keV/m

        gamma=self.E*1e3/0.511
        srloss=gamma*2*np.pi*self.K/self.lu
        srloss=-srloss*srloss*2/3*2.82e-15*511.   # SR loss in kev/m
        if self.planar:
            srloss*=0.5
        self.loss+=srloss
        

    def getEfficiency(self,comp,BW):
        self.eff=np.exp(-(self.loss-comp)**2/BW/BW/2.)
#


#------------------------------------
# Generic Ming Xie Model


    def MingXie(self,lam, aw,lu,beta,gamma,cur,siggam,emit,harm,isplanar):
        # calculates the gain length using the MingXie model
        # input:
        # aw - rms undulator field
        # lu - undulator period length
        # beta - mean beta function
        # gamma - beam energy
        # cur - beam current
        # siggam - rms energy spread in units of gamma
        # harm - harmonic number
        # isplanar - flag for using a planar undulator
        rho=self.getFELParameter(cur,gamma,emit,beta,aw,lu,harm,isplanar)

        pi=2.*math.asin(1)
        Lg=lu/(4*pi*np.sqrt(3)*rho)

        fch=self.getCoupling(aw,harm,isplanar)
        fc1=self.getCoupling(aw,1,isplanar)
        cor=math.pow((fc1/fch)**2/harm,1./3.)

        sigx=np.sqrt(emit*beta/gamma);
        sige=siggam/gamma;

        ed=Lg/(4*pi*sigx*sigx/lam)*cor/harm
        ee=Lg*4*pi*emit/(beta*lam*gamma)*cor*harm
        eg=Lg*4*pi*sige/lu*cor*harm

        #print(ed,ee,eg)

        eta=self.MingFit(ed,ee,eg)
        Lg3D=Lg*(1+eta)
        rho3D=rho/(1+eta)
        pbeam=gamma*511000*cur
        psat=1.6*rho*pbeam*Lg*Lg/Lg3D/Lg3D
        Ne=cur*lam/3e8/1.6e-19;
        P0=3*np.sqrt(4*pi)*rho3D*rho3D*pbeam/Ne/np.sqrt(math.log(Ne/rho3D))
        Lsat=Lg3D*math.log(9*psat/P0)
        BW=2*rho3D*np.sqrt(2*math.log(2))
        result={'rho3D':rho3D,'Lg3D':Lg3D,'Psat':psat,'Lsat':Lsat,'BW':BW}
        return result
 


    def MingFit(self,ed,ee,eg):
    # do the Ming Xie Fit    
        a1=0.45;
        a2=0.57;
        a3=0.55;
        a4=1.6;
        a5=3;
        a6=2;
        a7=0.35;
        a8=2.9;
        a9=2.4;
        a10=51;
        a11=0.95;
        a12=3;
        a13=5.4;
        a14=0.7;
        a15=1.9;
        a16=1140;
        a17=2.2;
        a18=2.9;
        a19=3.2;

        eta=a1*ed**a2 + a3*ee**a4 + a5*eg**a6;
        eta=eta + a7*ee**a8*eg**a9 + a10*ed**a11*eg**a12 + a13*ed**a14*ee**a15;
        eta=eta + a16*ed**a17*ee**a18*eg**a19;


        return eta



    def getFELParameter(self,I,gamma,emit,beta,aw,lu,harm,isplanar):
    # calculates 1D FEL parameter

        Ia=17045.;
        sigx=np.sqrt(emit*beta/gamma);
        fc = self.getCoupling(aw,harm,isplanar);
        pi=2*math.asin(1)
        rho=harm*I/Ia*(lu*fc*aw*0.5/pi/sigx)**2;
        rho=math.pow(rho,1./3.)*0.5/gamma;
        return rho

    def getCoupling(self,aw,harm,isplanar):

        fc = 1
        if (harm % 2)==0:
            fc=0
            return fc
        if isplanar==False:    # helical
            if harm>1:
                fc=0
            return fc   

        h=(harm-1)/2;
        xi=harm*0.5*aw*aw/(1+aw*aw);
        fc = sp.jn(h,xi)-sp.jn(h+1,xi);

        return fc


#---------------------------------------------------------
#       generic resistive wall wakefield

    def WakeSingleRes(self,radius):

        if np.abs(radius-self.rold) < 1e-6:
            return
        self.rold=radius

        conductivity=5.813e7
        relaxation=8.1e-6
        vacimp=377.
        c=3e8
        e=1.6e-19

        s0=(2*radius*radius/vacimp/conductivity)**(1./3.)
        gamma=relaxation/s0;
        coef = radius/(s0*s0);

#        print('Calculating Wake')
#        print('Radius:',radius)
#        print('s0',s0)

        kappamax=200
        nk=5000
        nq=1000
        kappa=np.linspace(1,nk+1,num=nk)*kappamax/nk
        Zre=kappa*0;
        Zim=kappa*0;

        if self.isRound:
            t = kappa/np.sqrt(1+kappa*kappa*gamma*gamma)
            lambdaRe=coef*np.sqrt(t)*np.sqrt(1.-t*gamma)
            lambdaIm=coef*np.sqrt(t)*np.sqrt(1.+t*gamma)-kappa*kappa*radius*0.5/s0/s0
            nomi=2.*kappa/(3e8*radius*s0)/(lambdaRe*lambdaRe+lambdaIm*lambdaIm)
            Zre=lambdaRe*nomi;
            Zim=-lambdaIm*nomi;
        else:
            dq=np.linspace(0,nq-1,num=nq)*15/(nq-1)
            dq[0]=1    # avoid singularity
            expval1=np.exp(dq)
            expval2=np.exp(-dq)
            coh=0.5*(expval1+expval2)
            sih=(coh-expval2)/dq
            coh[0]=1
            sih[0]=1.
            for i in range(0,nk):
                kap=kappa[i]
                t = kap/np.sqrt(1+kap*kap*gamma*gamma)
                scale=2.*15.*kap/(3e8*radius*s0*(2*nq-1))
                lambdaRe=coef*np.sqrt(t)*np.sqrt(1.-t*gamma)*(coh*coh)
                lambdaIm=coef*np.sqrt(t)*np.sqrt(1.+t*gamma)*(coh*coh)-kap*kap*radius*0.5/s0/s0*(sih*coh)
                nomi=scale/(lambdaRe*lambdaRe+lambdaIm*lambdaIm)
                Zre[i]=np.sum(lambdaRe*nomi)
                Zim[i]=-np.sum(lambdaIm*nomi);	

  
#        plt.plot(kappa, Zre)
#        plt.plot(kappa, Zim)
#        plt.xlim(0,4)
#        plt.xlabel('kappa')
#        plt.ylabel('Z')
#        plt.grid(True)
#        plt.show()

 
        self.ds=s0/200
        smax=100e-6   # no bunch should be longer
        ns=int(np.floor(smax/self.ds))
        self.s=np.linspace(0,smax,num=ns)
        self.wake=self.s*0
        phi=kappa/s0*self.ds
        for i in range(0,ns):
            self.wake[i]=np.sum(Zre*np.cos(i*phi)+Zim*np.sin(i*phi))
        coef=-kappamax/nk/s0*c/math.pi*(vacimp*e*c/4/math.pi)
        self.wake*=coef
 

#        plt.plot(self.s, self.wake)
#        plt.xlim(0,20e-6)
#        plt.xlabel('s')
#        plt.ylabel('W')
#        plt.grid(True)
#        plt.show()







