function aw = getResonantAw(lu,gamma,lambda)
aw=2*gamma*gamma*lambda/lu -1.;
if (aw > 0)
    aw=sqrt(aw);
else
    aw=0;
end
