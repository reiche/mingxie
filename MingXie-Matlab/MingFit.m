function eta = MingFit(ed,ee,eg)

a1=0.45;
a2=0.57;
a3=0.55;
a4=1.6;
a5=3;
a6=2;
a7=0.35;
a8=2.9;
a9=2.4;
a10=51;
a11=0.95;
a12=3;
a13=5.4;
a14=0.7;
a15=1.9;
a16=1140;
a17=2.2;
a18=2.9;
a19=3.2;

eta=a1*ed^a2 + a3*ee^a4 + a5*eg^a6;
eta=eta + a7*ee^a8*eg^a9 + a10*ed^a11*eg^a12 + a13*ed^a14*ee^a15;
eta=eta + a16*ed^a17*ee^a18*eg^a19;

