function [Lg3D psat rho eta] = MingXie(aw,lu,beta,gamma,cur,siggam,emit,harm,isplanar)
% calculates the gain length using the MingXie model
% input:
% aw - rms undulator field
% lu - undulator period length
% beta - mean beta function
% gamma - beam energy
% cur - beam current
% siggam - rms energy spread in units of gamma
% harm - harmonic number
% is planar - flag for using a planar undulator

lam=getResonantWavelength(lu,aw,gamma);
rho=getFELParameter(cur,gamma,emit,beta,aw,lu,harm,isplanar);

Lg=lu/(4*pi*sqrt(3)*rho);

fprintf('Wavelength (nm): %f\n',lam*1e9/harm);
fprintf('1D Gain Length (nm): %f\n',Lg);

fch=getCoupling(aw,harm,isplanar);
fc1=getCoupling(aw,1,isplanar);


cor=((fc1/fch)^2/harm)^(1./3.);


sigx=sqrt(emit*beta/gamma);
sige=siggam/gamma;

ed=Lg/(4*pi*sigx*sigx/lam)*cor/harm;
ee=Lg*4*pi*emit/(beta*lam*gamma)*cor*harm;
eg=Lg*4*pi*sige/lu*cor*harm;

eta=MingFit(ed,ee,eg);
Lg3D=Lg*(1+eta);

fprintf('3D Gain Length (nm): %f\n',Lg3D);


pbeam=gamma*511000*cur;
psat=1.6*rho*pbeam*Lg*Lg/Lg3D/Lg3D;

end

 