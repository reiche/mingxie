function lambda = getResonantWavelength(lu,aw,gamma)
lambda=lu*0.5*(1.+aw*aw)/gamma/gamma;
