function energy = getResonantEnergy(lu,aw,lambda)
energy=0.5*lu/lambda*(1+aw*aw);
if (energy > 1)
    energy=sqrt(energy);
else
    energy=-1;
end
