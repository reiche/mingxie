function fc = getCoupling(aw,harm,isplanar)
fc = 1;
if mod(harm,2)==0
    fc=0;
    return;
end

% get the coupling factor in case of planar undulator
if (isplanar==0)
    return
else
    h=(harm-1)/2;
    xi=harm*0.5*aw*aw/(1+aw*aw);
    fc = besselj(h,xi)-besselj(h+1,xi);
end
