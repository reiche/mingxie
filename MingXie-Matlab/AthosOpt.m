function AthosOpt
[lam1 E1 dE1 Lg1 P1]=OptimizeLam(1.1);
[lam2 E2 dE2 Lg2 P2]=OptimizeLam(1.0);

lref=1.257;
figure(1)
plot(lam1,E1,'r',lam2,E2,'b')

xlabel('\lambda_u (cm)')
ylabel('E_{inj} (GeV)')


figure(2)
plot(lam1,dE1,'r',lam2,dE2,'b')

xlabel('\lambda_u (cm)')
ylabel('\Delta E (GeV)')

figure(3)
plot(lam1,(Lg1-lref)/lref*100,'r',lam2,(Lg2-lref)/lref*100,'b')
xlabel('\lambda_u (cm)')
ylabel('\Delta L_g/L_g (%)')

figure(4)
plot(lam1,P1*1e-9,'r',lam2,P2*1e-9,'b')
xlabel('\lambda_u (cm)')
ylabel('P_{sat} (GW)')

plotScan(3.5,10)
plotScan(3.8,20)
plotScan(0,30)
end

function [lamu Einj dE Lg3D psat]=OptimizeLam(K)

lref=1.257;
Kmin=K;
N=100;
lamu=((1:N)/N)*0.04+0.02;
lamu=lamu*100; % in cm
Emax=sqrt(0.01*lamu/2/0.7e-9*(1+0.5*Kmin*Kmin))*0.511e-3;
dE=Emax-sqrt(0.01/2/7e-9*lamu.*(1+0.5*0.95*0.95*lamu.*lamu))*0.511e-3;
Lg3D=1:N;
psat=1:N;
for  i=1:N
   [Lg3D(i) psat(i) rho eta] = MingXie(Kmin,lamu(i)*0.01,10,Emax(i)*1e3/0.511,2000,250/511,0.3e-6,0);
end
Einj=Emax-dE*0.5;
dE=0.5*dE;

end

function plotScan(Emax,ff)
lref=1.257;

[lam1 gamma1 K1 psat1 lg1] = gaincurve(0.038, 3.8*0.95,1.1,Emax);
[lam2 gamma2 K2 psat2 lg2] = gaincurve(0.040, 4.0*0.95,1.1,Emax);
[lam3 gamma3 K3 psat3 lg3] = gaincurve(0.042, 4.2*0.95,1.1,Emax);
[lam4 gamma4 K4 psat4 lg4] = gaincurve(0.044, 4.4*0.95,1.1,Emax);
[lam5 gamma5 K5 psat5 lg5] = gaincurve(0.046, 4.6*0.95,1.1,Emax);

lg1=(lg1-lref)/lref*100;
lg2=(lg2-lref)/lref*100;
lg3=(lg3-lref)/lref*100;
lg4=(lg4-lref)/lref*100;
lg5=(lg5-lref)/lref*100;

E1=1.24./lam1;
E2=1.24./lam2;
E3=1.24./lam3;
E4=1.24./lam4;
E5=1.24./lam5;
lam1=E1;
lam2=E2;
lam3=E3;
lam4=E4;
lam5=E5;

figure(ff)
subplot(2,2,1)
plot(lam1,gamma1,'r',lam2,gamma2,'b',lam3,gamma3,'g',lam4,gamma4,'m',lam5,gamma5,'k');
xlabel('E_{ph} (keV)')
ylabel('E (GeV)')

subplot(2,2,2)
plot(lam1,K1,'r',lam2,K2,'b',lam3,K3,'g',lam4,K4,'m',lam5,K5,'k');
xlabel('E_{ph} (keV)')
ylabel('K')

subplot(2,2,3)
plot(lam1,psat1,'r',lam2,psat2,'b',lam3,psat3,'g',lam4,psat4,'m',lam5,psat5,'k');
xlabel('E_{ph} (keV)')
ylabel('P (GW)')

subplot(2,2,4)
plot(lam1,lg1,'r',lam2,lg2,'b',lam3,lg3,'g',lam4,lg4,'m',lam5,lg5,'k');
xlabel('E_{ph} (keV)')
ylabel('\Delta L_g/L_g (%)')

end

function [lam gamma K psat lgain] = gaincurve(lu_in,Kmax_in,Kref,Emax)

N=200;
lam=6.4e-9*(0:(N-1))/(N-1)+6.2e-10;


lammin=0.7e-9;

Kmax=Kmax_in;
Kmin=Kref;
lu=lu_in;
beta=10;
emit=0.3e-6;

gamma0=sqrt((1+0.5*Kmin*Kmin)*0.5*lu/lammin);
if Emax>0
    gamma0=Emax/0.511*1e3;
end

lamcut=0.5*lu*(1+0.5*Kmax*Kmax)/gamma0/gamma0;


gamma=sqrt((1+0.5*Kmax*Kmax)*0.5*lu./lam);
idx=find(lam<lamcut);
gamma(idx)=gamma0;

K=sqrt((2*lam.*gamma.*gamma/lu-1)*2);

psat=(1:N);
Lg3D=psat;

for i=1:N
[Lg3D(i) psat(i) rho eta] = MingXie(K(i),lu,beta,gamma(i),2000,250/511,emit,0);
end

lam=lam*1e9;
gamma=gamma*0.511*1e-3;
psat=psat*1e-9;
lgain=Lg3D;

return
end


