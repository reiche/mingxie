function rho = getFELParameter(I,gamma,emit,beta,aw,lu,harm,isplanar)
% get the FEL parameter
Ia=17045.;
sigx=sqrt(emit*beta/gamma);
fc = getCoupling(aw,harm,isplanar);
rho=harm*I/Ia*(lu*fc*aw*0.5/pi/sigx)^2;
rho=rho^(1./3.)*0.5/gamma;

