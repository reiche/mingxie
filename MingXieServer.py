#!/opt/gfa/python-3.5/latest/bin/python 

 # server to check status of bunch compressor and synchronize the values 

from datetime import datetime

import time
import queue
import threading
import logging

#  modules for interface with epics and the softIOC package
from pcaspy import Driver, SimpleServer, Alarm, Severity
import sys
import socket
from epics import caget,caput,PV

import numpy as np

# program specific modules - transformation between moto position etc.

from MingXie import MingXie



# the channels for the server

prefix = 'SF-MODEL:'
pvdb = {
    'ARAMIS-PHOTONENERGY': {'prec': 3, 'scan' : 1, 'unit' : 'keV'},
    'ARAMIS-WAVELENGTH': {'prec': 3, 'scan' : 1, 'unit' : 'A'},
    'ARAMIS-K'         : {'prec': 3, 'scan' : 1, 'unit' : '', 'value': 1.2},
    'ARAMIS-GAP'       : {'prec': 3, 'scan' : 1, 'unit' : 'mm',},
    'ARAMIS-ENERGY'    : {'prec': 3, 'scan' : 1, 'unit' : 'GeV', 'value': 5.8},
    'ARAMIS-CURRENT'   : {'prec': 3, 'scan' : 1, 'unit' : 'kA', 'value': 3.0},
    'ARAMIS-CHARGE'    : {'prec': 3, 'scan' : 1, 'unit' : 'pC', 'value': 200},
    'ARAMIS-SPREAD'    : {'prec': 3, 'scan' : 1, 'unit' : '%', 'value': 0.01},
    'ARAMIS-EMITTANCE' : {'prec': 3, 'scan' : 1, 'unit' : 'nm', 'value': 300},
    'ARAMIS-BETA'      : {'prec': 3, 'scan' : 1, 'unit' : 'm', 'value': 10},
    'ARAMIS-TUNING'    : {'type': 'enum', 'scan' : 1, 'enums': ['Max K-Value','K-Value','Energy']},

    'ARAMIS-GAINLENGTH': {'prec': 3, 'scan' : 1, 'unit' : 'm'},
    'ARAMIS-SATLENGTH' : {'prec': 3, 'scan' : 1, 'unit' : 'm'},
    'ARAMIS-SATPOWER'  : {'prec': 3, 'scan' : 1, 'unit' : 'GW'},
    'ARAMIS-RHO'       : {'prec': 3, 'scan' : 1, 'unit' : ''},
    'ARAMIS-BANDWIDTH' : {'prec': 3, 'scan' : 1, 'unit' : '%'},
    'ARAMIS-PULSEENERGY' : {'prec': 3, 'scan' : 1, 'unit' : 'uJ'},


    'ARAMIS-S'         : {'prec': 3, 'type': 'float', 'count': 3000, 'unit' : 'm'},
    'ARAMIS-LOSS'      : {'prec': 3, 'type': 'float', 'count': 3000, 'unit' : 'keV/m'},
    'ARAMIS-EFFICIENCY': {'prec': 3, 'type': 'float', 'count': 3000, 'unit' : '%'},
    'ARAMIS-EFFICIENCY_OPT' : {'prec': 3, 'type': 'float', 'count': 3000, 'unit' : '%'},

    'ARAMIS-EFFICIENCY_TAP' : {'prec': 3, 'type': 'float', 'count': 100, 'unit' : '%'},
    'ARAMIS-EFFICIENCY_COMP' : {'prec': 3, 'type': 'float', 'count': 100, 'unit' : '%'},


    'ARAMIS-COMPENSATION'   : {'prec': 3, 'scan' : 1, 'unit' : 'keV/m', 'value': -200},
    'ARAMIS-COMPENSATION_OPT'   : {'prec': 3, 'scan' : 1, 'unit' : 'keV/m'},
    'ARAMIS-TAPER_OPT'   : {'prec': 3, 'scan' : 1, 'unit' : '%'},



    'ATHOS-PHOTONENERGY': {'prec': 3, 'scan' : 1, 'unit' : 'keV'},
    'ATHOS-WAVELENGTH': {'prec': 3, 'scan' : 1, 'unit' : 'A'},
    'ATHOS-K'         : {'prec': 3, 'scan' : 1, 'unit' : '', 'value': 3},
    'ATHOS-GAP'       : {'prec': 3, 'scan' : 1, 'unit' : 'mm',},
    'ATHOS-ENERGY'    : {'prec': 3, 'scan' : 1, 'unit' : 'GeV', 'value': 3},
    'ATHOS-CURRENT'   : {'prec': 3, 'scan' : 1, 'unit' : 'kA', 'value': 3},
    'ATHOS-CHARGE'    : {'prec': 3, 'scan' : 1, 'unit' : 'pC', 'value': 200},
    'ATHOS-SPREAD'    : {'prec': 3, 'scan' : 1, 'unit' : '%', 'value': 0.01},
    'ATHOS-EMITTANCE' : {'prec': 3, 'scan' : 1, 'unit' : 'nm', 'value': 300},
    'ATHOS-BETA'      : {'prec': 3, 'scan' : 1, 'unit' : 'm', 'value': 10},
    'ATHOS-TUNING'    : {'type': 'enum', 'scan' : 1, 'enums': ['Max K-Value','K-Value','Energy']},
    'ATHOS-POLARIZATION': {'type': 'enum', 'scan' : 1, 'enums': ['Planar','Helical']},

    'ATHOS-GAINLENGTH': {'prec': 3, 'scan' : 1, 'unit' : 'm'},
    'ATHOS-SATLENGTH' : {'prec': 3, 'scan' : 1, 'unit' : 'm'},
    'ATHOS-SATPOWER'  : {'prec': 3, 'scan' : 1, 'unit' : 'GW'},
    'ATHOS-RHO'       : {'prec': 3, 'scan' : 1, 'unit' : ''},
    'ATHOS-BANDWIDTH' : {'prec': 3, 'scan' : 1, 'unit' : '%'},
    'ATHOS-PULSEENERGY' : {'prec': 3, 'scan' : 1, 'unit' : 'uJ'},


    'ATHOS-S'         : {'prec': 3, 'type': 'float', 'count': 3000, 'unit' : 'm'},
    'ATHOS-LOSS'      : {'prec': 3, 'type': 'float', 'count': 3000, 'unit' : 'keV/m'},
    'ATHOS-EFFICIENCY': {'prec': 3, 'type': 'float', 'count': 3000, 'unit' : '%'},
    'ATHOS-EFFICIENCY_OPT' : {'prec': 3, 'type': 'float', 'count': 3000, 'unit' : '%'},

    'ATHOS-EFFICIENCY_TAP' : {'prec': 3, 'type': 'float', 'count': 100, 'unit' : '%'},
    'ATHOS-EFFICIENCY_COMP' : {'prec': 3, 'type': 'float', 'count': 100, 'unit' : '%'},

    'ATHOS-COMPENSATION'   : {'prec': 3, 'scan' : 1, 'unit' : 'keV/m', 'value': -200},
    'ATHOS-COMPENSATION_OPT'   : {'prec': 3, 'scan' : 1, 'unit' : 'keV/m'},
    'ATHOS-TAPER_OPT'   : {'prec': 3, 'scan' : 1, 'unit' : '%'},


    'PING'      : {'type': 'string', 'scan' : 1,},
    'STOP'      : {'type': 'int'}
}

class FELPerformance(Driver):
    def  __init__(self):

        super(FELPerformance, self).__init__()

        self.version='2.0.1'
        self.program='MingXie'

        # enable logging
        self.logfilename="/sf/data/applications/BD-MingXie/MingXieServer.log"
        logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%m-%d %H:%M:%S',
                    filename=self.logfilename,
                    filemode='w')

        self.logger=logging.getLogger(self.program)
        self.logger.info('started at %s' % datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
        self.logger.info('Version: %s ' % self.version)
        self.logger.info('Host: %s' % socket.gethostname())
        self.running=True
        


        self.ns = 1000  # number of point for wake calculations

        # set-up the Model for Armais and Athos
        self.MingXie={}

        self.MingXie['Athos']=MingXie()
        self.MingXie['Aramis']=MingXie()

        self.MingXie['Aramis'].init(0.015,2.1,6.1,0.8, 1.6,4,False)   # period length, min energy, max energy, min K, max K
        self.MingXie['Aramis'].K=1.2
        self.MingXie['Aramis'].E=5.8
        self.updateResonance('Aramis')

        self.MingXie['Athos'].init(0.038,2.8,3.4,0.8, 3.7,2,True)   # period length, min energy, max energy, min K, max K
        self.MingXie['Athos'].K=3.
        self.MingXie['Athos'].E=3.
        self.updateResonance('Athos')


    def getGap(self,target,K):
        if target is 'Aramis':
            K0=5.9
            a=-6.43
            b=3.85
            c1=a/b
            c2=-np.log(K/K0)/b
            arg=np.sqrt(0.25*c1*c1-c2)
            x1=(0.5*c1+arg)*15e-3
            x2=(0.5*c1-arg)*15e-3
            return np.abs(x1)*0.5
        else:
            return 2.5e-3


    def setK(self,target,val):
        self.MingXie[target].K=val
        self.updateResonance(target)

    def setE(self,target,val):
        self.MingXie[target].E=val
        self.updateResonance(target)

    def setEph(self,target,val):
        if target is 'Aramis':
            strat=self.getParam('ARAMIS-TUNING')
        else:
            strat=self.getParam('ATHOS-TUNING')
        select=['M','K','E']
        suc=self.MingXie[target].findResonance(val,select[strat])
        if suc:
            self.updateResonance(target)

# updating the model

    def updateResonance(self,target):
        self.setParam('%s-K' % target.upper(),self.MingXie[target].K)
        self.setParam('%s-ENERGY' %target.upper(),self.MingXie[target].E)
        rad=self.getGap(target,self.MingXie[target].K)
        self.setParam('%s-GAP' %target.upper(),2*rad*1e3)
        self.MingXie[target].WakeSingleRes(rad)
        Eph=self.MingXie[target].getResonantEnergy()
        self.setParam('%s-PHOTONENERGY' % target.upper(),Eph)
        lam=12.39842/Eph
        self.setParam('%s-WAVELENGTH' % target.upper(),lam)
        self.updateMingXie(target)  # update the Ming Xie model


    def updateMingXie(self,target):
        cur=self.getParam('%s-CURRENT' % target.upper())
        emit=self.getParam('%s-EMITTANCE' % target.upper())
        spread=self.getParam('%s-SPREAD' % target.upper())
        beta=self.getParam('%s-BETA' % target.upper())
        charge=self.getParam('%s-CHARGE' % target.upper())  
        res=self.MingXie[target].getMingXie(cur,emit,spread,beta)
        self.setParam('%s-GAINLENGTH' % target.upper(), res['Lg3D'])
        self.setParam('%s-SATLENGTH' % target.upper(), res['Lsat'])
        self.setParam('%s-SATPOWER' % target.upper(), res['Psat']*1e-9)
        self.setParam('%s-RHO' % target.upper(), res['rho3D'])
        self.setParam('%s-BANDWIDTH' % target.upper(), res['BW']*100)
        
        self.updateWake(target)


    def updateWake(self,target):

        charge=self.getParam('%s-CHARGE' % target.upper())*1e-12  
        cur=self.getParam('%s-CURRENT' % target.upper())*1e3
        Psat=self.getParam('%s-SATPOWER' % target.upper())*1e9
        smax=charge/cur*3e8;
        Epulse=Psat*smax/3e8*1e6;
        profile=np.linspace(0,self.ns-1,num=self.ns)*0+cur
        s=np.linspace(0,smax,num=self.ns)
        self.MingXie[target].getLoss(profile,s)
        loss=self.MingXie[target].loss

        # optimizing taper
        E=self.getParam('%s-ENERGY' % target.upper())*1e6    # in keV
        BW=self.getParam('%s-BANDWIDTH' % target.upper())/100.  # away from percent
        Ls=self.getParam('%s-SATLENGTH' % target.upper())
        tol=E*BW/Ls

        lmin=np.min(loss)
        lmax=np.max(loss)
        efftap=np.linspace(lmin-100,lmax+100,num=100)
        effres=efftap*0
        for i in range(100):
            self.MingXie[target].getEfficiency(efftap[i],tol)
            effres[i]=np.mean(self.MingXie[target].eff)*100

        comp=self.getParam('%s-COMPENSATION' % target.upper())
        self.MingXie[target].getEfficiency(comp,tol)
        eff=self.MingXie[target].eff*100

        comp_opt= efftap[np.argmax(effres)]

        self.MingXie[target].getEfficiency(comp_opt,tol)
        eff_opt=self.MingXie[target].eff*100
        Epulse*=np.mean(eff_opt)/100

        dE=comp_opt*self.MingXie[target].lmod/E 
        K=self.getParam('%s-K' % target.upper())
        K2=K*K
        if self.MingXie[target].planar:
            K2*=0.5
        dK=(1+K2)/K*dE

        self.setParam('%s-S' % target.upper(),s)
        self.setParam('%s-LOSS' % target.upper(),loss)
        self.setParam('%s-PULSEENERGY' % target.upper(),Epulse)
        self.setParam('%s-EFFICIENCY' % target.upper(),eff)
        self.setParam('%s-EFFICIENCY_TAP' % target.upper(),effres)
        self.setParam('%s-EFFICIENCY_COMP' % target.upper(),efftap)
        self.setParam('%s-EFFICIENCY_OPT' % target.upper(),eff_opt)
        self.setParam('%s-COMPENSATION_OPT' % target.upper(),comp_opt)
        self.setParam('%s-TAPER_OPT' % target.upper(),dK)

        

#----------------------------------------
# event haendler for read and write
 
    def read(self, reason):
        if reason =='PING':
            return datetime.now().strftime('Alive at %Y-%m-%d %H:%M:%S')

        value = self.getParam(reason)
        return value

 
    def write(self,reason,value):

        if reason == 'STOP':
           if value > 0: 
               self.running=False
           return    

        target='Aramis'
        if 'ATHOS' in reason:
            target='Athos'

        if '-K' in reason:
            self.setK(target,value)
            return
        if '-ENERGY' in reason:
            self.setE(target,value)
            return
        if '-PHOTONENERGY' in reason:
            self.setEph(target,value)
            return


        self.setParam(reason,value)
        if '-POLARIZATION' in reason:
            if value is 0:
                self.MingXie[target].planar=True
            else:
                self.MingXie[target].planar=False
            self.updateResonance(target)


        if '-CURRENT' in reason or '-EMIT' in reason or '-SPREAD' in reason or '-BETA' in reason:
            self.updateMingXie(target)

        if '-COMPENSATION' in reason or '-CHARGE' in reason:
            self.updateWake(target)

 
    def disconnect(self):
        self.queuerunning=False
        self.logger.info('terminated at %s' % datetime.now().strftime('%Y-%m-%d %H:%M:%S)'))

    def __del__(self):
        self.disconnect()






if __name__ == '__main__':
    server = SimpleServer()
    server.createPV(prefix, pvdb)
    driver = FELPerformance()

    # process CA transactions
    while driver.running:
        try:
            server.process(0.1)
        except KeyboardInterrupt:
            driver.disconnect()
            print('Gracefully exiting...')
            sys.exit()





